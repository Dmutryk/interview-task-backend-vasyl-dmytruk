<?php

declare(strict_types=1);

namespace Tests\Feature\Invoice;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;
use App\Modules\Invoices\Domain\Models\Company;
use App\Modules\Invoices\Domain\Models\Invoice;
use Illuminate\Http\Response;
use Tests\TestCase;
use App\Modules\Invoices\Domain\Models\Product;

class ShowTest extends TestCase
{
    use RefreshDatabase;

    public function test_show_endpoint(): void
    {
        $company = Company::factory()->create();
        $billedCompany = Company::factory()->create();

        $invoice = Invoice::factory()
            ->create([
                'company_id' => $company->id,
                'billed_company_id' => $billedCompany->id,
            ]);

        $products = Product::factory()
            ->count(5)
            ->create();

        $lines = $products->map(function ($product) use ($invoice): array {
            return [
                'id' => Uuid::uuid4()->toString(),
                'invoice_id' => $invoice->id,
                'product_id' => $product->id,
                'quantity' => rand(1, 100),
                'created_at' => now(),
                'updated_at' => now(),
            ];
        });

        DB::table('invoice_product_lines')->insert($lines->toArray());

        $response = $this->get('/api/invoices/' . $invoice->id);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonFragment([
                'number' => $invoice->number,
                'company' => [
                    'id' => $company->id,
                    'name' => $company->name,
                    'street' => $company->street,
                    'city' => $company->city,
                    'zip' => $company->zip,
                    'phone' => $company->phone,
                    'email' => $company->email,
                    'created_at' => $company->created_at,
                    'updated_at' => $company->updated_at,
                ],
                'billed_company' => [
                    'id' => $billedCompany->id,
                    'name' => $billedCompany->name,
                    'street' => $billedCompany->street,
                    'city' => $billedCompany->city,
                    'zip' => $billedCompany->zip,
                    'phone' => $billedCompany->phone,
                    'email' => $billedCompany->email,
                    'created_at' => $billedCompany->created_at,
                    'updated_at' => $billedCompany->updated_at,
                ],
            ])->assertJsonCount(5, 'data.products');
    }
}
