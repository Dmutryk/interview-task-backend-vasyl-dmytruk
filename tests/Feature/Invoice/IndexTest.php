<?php

declare(strict_types=1);

namespace Tests\Feature\Invoice;

use App\Modules\Invoices\Domain\Models\Company;
use App\Modules\Invoices\Domain\Models\Invoice;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class IndexTest extends TestCase
{
    use RefreshDatabase;

    public function test_index_endpoint(): void
    {
        $company = Company::factory()->create();
        $billedCompany = Company::factory()->create();

        Invoice::factory()
            ->for($company)
            ->for($billedCompany)
            ->count(30)
            ->create();

        $response = $this->get('/api/invoices?per_page=25');

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonCount(25, 'data');
    }
}
