<?php

declare(strict_types=1);

namespace App\Infrastructure\DTO;

use Illuminate\Http\Request;

abstract class DataTransferObject
{
    public function __construct(array $parameters = [])
    {
        foreach ($parameters as $property => $value) {
            if (property_exists(static::class, $property)) {
                $this->{$property} = $value;
            }
        }
    }

    abstract public static function fromRequest(Request $request): static;
}
