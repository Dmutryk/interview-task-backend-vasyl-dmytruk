<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Database\Factories;

use App\Modules\Invoices\Domain\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Ramsey\Uuid\Uuid;

class ProductFactory extends Factory
{
    protected $model = Product::class;

    public function definition(): array
    {
        $productNames = [
            'pen',
            'pencil',
            'razer',
            'gum',
            'towel',
            'backpack',
            'book',
            'shoes',
            'trousers',
            't-shirt',
            'snickers',
            'water',
            'coca-cola',
            'pepsi',
        ];

        return [
            'id' => Uuid::uuid4()->toString(),
            'name' => $productNames[array_rand($productNames)],
            'price' => rand(1, 99),
            'currency' => 'usd',
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
