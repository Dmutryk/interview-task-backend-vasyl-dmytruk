<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Dto;

use App\Infrastructure\DTO\DataTransferObject;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class InvoiceDto extends DataTransferObject
{
    public ?UuidInterface $id;

    public function defaults(): array
    {
        return [
            'id' => null,
        ];
    }

    public static function fromRequest(Request $request): static
    {
        return new static([
            'id' => Uuid::fromString($request->route('id')),
        ]);
    }
}
