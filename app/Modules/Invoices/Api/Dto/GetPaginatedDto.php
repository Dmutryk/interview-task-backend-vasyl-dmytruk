<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Dto;

use App\Infrastructure\DTO\DataTransferObject;
use App\Modules\Invoices\Domain\Repositories\InvoiceRepository;
use Illuminate\Http\Request;

class GetPaginatedDto extends DataTransferObject
{
    public int $page;

    public int $per_page;

    public static function fromRequest(Request $request): static
    {
        return new static([
            'page' => $request->get('page') ? (int)$request->get('page') : InvoiceRepository::PAGE_DEFAULT,
            'per_page' => $request->get('per_page') ? (int)$request->get('per_page') : InvoiceRepository::PER_PAGE_DEFAULT,
        ]);
    }

    protected function rules(): array
    {
        return [
            'page' => ['sometimes', 'required', 'integer'],
            'per_page' => [
                'sometimes',
                'required',
                'integer',
                "min:1",
                "max:" . InvoiceRepository::PER_PAGE_MAX,
            ],
        ];
    }
}
