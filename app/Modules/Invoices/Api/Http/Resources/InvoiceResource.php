<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Http\Resources;

use App\Modules\Invoices\Domain\Models\Invoice;
use App\Modules\Invoices\Domain\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Invoice */
class InvoiceResource extends JsonResource
{
    public function toArray($request): array
    {
        $payload = [
            'id' => $this->id,
            'number' => $this->number,
            'date' => $this->date,
            'due_date' => $this->due_date,
            'status' => $this->status,
            'company' => new JsonResource($this->whenLoaded('company')),
            'billed_company' => new JsonResource($this->whenLoaded('billedCompany')),
            'products' => ProductResource::collection($this->whenLoaded('products')),
        ];

        if ($this->relationLoaded('products')) {
            $payload['total_price'] = $this->products->sum(function (Product $product) {
                return $product->price * $product->pivot->quantity;
            });
        }

        return $payload;
    }
}
