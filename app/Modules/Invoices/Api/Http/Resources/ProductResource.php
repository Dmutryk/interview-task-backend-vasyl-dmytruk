<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Http\Resources;

use App\Modules\Invoices\Domain\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Product */
class ProductResource extends JsonResource
{
    public function toArray($request)
    {
        $total = $this->price * $this->pivot->quantity;

        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'currency' => $this->currency,
            'quantity' => $this->pivot->quantity,
            'total' => $total,
        ];
    }
}
