<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Http\Controllers;

use App\Infrastructure\Http\Controller;
use App\Modules\Invoices\Api\Dto\GetPaginatedDto;
use App\Modules\Invoices\Api\Dto\InvoiceDto;
use App\Modules\Invoices\Api\Exceptions\InvoiceNotFoundException;
use App\Modules\Invoices\Api\Http\Resources\InvoiceResource;
use App\Modules\Invoices\Api\Services\InvoiceService;
use App\Modules\Invoices\Modules\Approval\Application\Exceptions\StatusAlreadyAssignedException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class InvoiceController extends Controller
{
    public function __construct(
        private readonly InvoiceService $invoiceService
    ) {
    }

    public function index(Request $request): AnonymousResourceCollection
    {
        $dto = GetPaginatedDto::fromRequest($request);
        $data = $this->invoiceService->getAll($dto);

        return InvoiceResource::collection($data);
    }

    /**
     * @throws InvoiceNotFoundException
     */
    public function show(Request $request): InvoiceResource
    {
        $dto = InvoiceDto::fromRequest($request);
        $invoice = $this->invoiceService->findOne($dto);

        return new InvoiceResource($invoice);
    }

    /**
     * @throws InvoiceNotFoundException
     * @throws StatusAlreadyAssignedException
     */
    public function approve(Request $request): JsonResponse
    {
        $dto = InvoiceDto::fromRequest($request);
        $this->invoiceService->approve($dto);

        return new JsonResponse(['success' => true]);
    }

    /**
     * @throws InvoiceNotFoundException
     * @throws StatusAlreadyAssignedException
     */
    public function reject(Request $request): JsonResponse
    {
        $dto = InvoiceDto::fromRequest($request);
        $this->invoiceService->reject($dto);

        return new JsonResponse(['success' => true]);
    }
}
