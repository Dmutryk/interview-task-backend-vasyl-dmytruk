<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Exceptions;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class InvoiceNotFoundException extends \Exception
{
    public function render(Request $request): JsonResponse
    {
        return new JsonResponse(['message' => 'Invoice not found'], Response::HTTP_NOT_FOUND);
    }
}
