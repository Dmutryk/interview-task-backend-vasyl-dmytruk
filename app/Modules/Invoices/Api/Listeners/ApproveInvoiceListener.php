<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Listeners;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Domain\Repositories\InvoiceRepository;
use App\Modules\Invoices\Modules\Approval\Api\Events\EntityApproved;

final readonly class ApproveInvoiceListener
{
    public function __construct(private InvoiceRepository $invoiceRepository)
    {
    }

    public function handle(EntityApproved $event): void
    {
        $this->invoiceRepository->updateStatus(
            $event->approvalDto->id->toString(),
            StatusEnum::APPROVED->value
        );
    }
}
