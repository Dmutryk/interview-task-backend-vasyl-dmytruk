<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Services;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Api\Dto\GetPaginatedDto;
use App\Modules\Invoices\Api\Dto\InvoiceDto;
use App\Modules\Invoices\Api\Exceptions\InvoiceNotFoundException;
use App\Modules\Invoices\Domain\Models\Invoice;
use App\Modules\Invoices\Domain\Repositories\InvoiceRepository;
use App\Modules\Invoices\Modules\Approval\Api\Dto\ApprovalDto;
use App\Modules\Invoices\Modules\Approval\Application\ApprovalFacade;
use App\Modules\Invoices\Modules\Approval\Application\Exceptions\StatusAlreadyAssignedException;
use Illuminate\Pagination\LengthAwarePaginator;

readonly class InvoiceService
{
    public function __construct(
        private InvoiceRepository $repository,
        private ApprovalFacade $approvalFacade
    ) {
    }

    public function getAll(GetPaginatedDto $dto): LengthAwarePaginator
    {
        return $this->repository->getPaginated($dto);
    }

    /**
     * @throws InvoiceNotFoundException
     */
    public function findOne(InvoiceDto $dto): Invoice
    {
        return $this->getInvoice($dto->id->toString());
    }

    /**
     * @throws InvoiceNotFoundException
     * @throws StatusAlreadyAssignedException
     */
    public function approve(InvoiceDto $dto): void
    {
        $invoice = $this->getInvoice($dto->id->toString());

        $this->approvalFacade->approve(
            new ApprovalDto(
                $dto->id,
                StatusEnum::tryFrom($invoice->status),
                Invoice::ENTITY_NAME
            )
        );
    }

    /**
     * @throws InvoiceNotFoundException
     * @throws StatusAlreadyAssignedException
     */
    public function reject(InvoiceDto $dto): void
    {
        $invoice = $this->getInvoice($dto->id->toString());

        $this->approvalFacade->reject(
            new ApprovalDto(
                $dto->id,
                StatusEnum::tryFrom($invoice->status),
                Invoice::ENTITY_NAME
            )
        );
    }

    /**
     * @throws InvoiceNotFoundException
     */
    private function getInvoice(string $invoiceId): Invoice
    {
        $invoice = $this->repository->getById($invoiceId);

        if (is_null($invoice)) {
            throw new InvoiceNotFoundException();
        }

        return $invoice;
    }
}
