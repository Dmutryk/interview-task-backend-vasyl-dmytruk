<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Domain\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Modules\Invoices\Infrastructure\Database\Factories\InvoiceFactory;

/**
 * @property string $id
 * @property string $number
 * @property Carbon $date
 * @property Carbon $due_date
 * @property string $company_id
 * @property string $status
 * @property string $billed_company_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property-read Company $company
 * @property-read Company $billedCompany
 * @property-read Product $products
 */
class Invoice extends Model
{
    use HasUuids;
    use HasFactory;

    public const ENTITY_NAME = 'Invoice';

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    public function billedCompany(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'billed_company_id');
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'invoice_product_lines')->withPivot('quantity');
    }

    protected static function newFactory(): Factory
    {
        return InvoiceFactory::new();
    }
}
