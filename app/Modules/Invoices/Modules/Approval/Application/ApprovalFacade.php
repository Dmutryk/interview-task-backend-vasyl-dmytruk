<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Modules\Approval\Application;

use App\Domain\Enums\StatusEnum;
use Illuminate\Contracts\Events\Dispatcher;
use App\Modules\Invoices\Modules\Approval\Api\Dto\ApprovalDto;
use App\Modules\Invoices\Modules\Approval\Api\Events\EntityRejected;
use App\Modules\Invoices\Modules\Approval\Api\Events\EntityApproved;
use App\Modules\Invoices\Modules\Approval\Api\ApprovalFacadeInterface;
use App\Modules\Invoices\Modules\Approval\Application\Exceptions\StatusAlreadyAssignedException;

final readonly class ApprovalFacade implements ApprovalFacadeInterface
{
    public function __construct(
        private Dispatcher $dispatcher
    ) {
    }

    /**
     * @throws StatusAlreadyAssignedException
     */
    public function approve(ApprovalDto $entity): true
    {
        $this->validate($entity);
        $this->dispatcher->dispatch(new EntityApproved($entity));

        return true;
    }

    /**
     * @throws StatusAlreadyAssignedException
     */
    public function reject(ApprovalDto $entity): true
    {
        $this->validate($entity);
        $this->dispatcher->dispatch(new EntityRejected($entity));

        return true;
    }

    /**
     * @throws StatusAlreadyAssignedException
     */
    private function validate(ApprovalDto $dto): void
    {
        if (StatusEnum::DRAFT !== StatusEnum::tryFrom($dto->status->value)) {
            throw new StatusAlreadyAssignedException();
        }
    }
}
