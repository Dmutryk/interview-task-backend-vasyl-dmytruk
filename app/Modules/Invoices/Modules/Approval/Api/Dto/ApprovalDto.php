<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Modules\Approval\Api\Dto;

use Ramsey\Uuid\UuidInterface;
use App\Domain\Enums\StatusEnum;

final readonly class ApprovalDto
{
    /** @param class-string $entity */
    public function __construct(
        public UuidInterface $id,
        public StatusEnum $status,
        public string $entity,
    ) {
    }
}
