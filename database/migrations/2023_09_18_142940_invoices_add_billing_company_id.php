<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        if (Schema::hasTable('invoices')) {
            Schema::table('invoices', static function (Blueprint $table): void {
                $table->uuid('billed_company_id')->nullable();
                $table->foreign('billed_company_id', 'invoices_billed_company_id_foreign')
                    ->references('id')
                    ->on('companies');
            });
        }
    }

    public function down(): void
    {
        if (Schema::hasColumn('invoices', 'billed_company_id')) {
            Schema::table('invoices', static function (Blueprint $table): void {
                $table->dropForeign('invoices_billed_company_id_foreign');
                $table->dropColumn('billed_company_id');
            });
        }
    }
};
