<?php

use App\Modules\Invoices\Api\Http\Controllers\InvoiceController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(InvoiceController::class)->prefix('invoices')->group(function() {
    Route::get('', 'index');
    Route::get('/{id}', 'show');
    Route::post('/{id}/approve', 'approve');
    Route::post('/{id}/reject', 'reject');
});
