container := workspace
db_container := mariadb
seeder := "App\Modules\Invoices\Infrastructure\Database\Seeders\DatabaseSeeder"

migrate:
	cd ./docker/ && docker compose exec $(container) php artisan migrate;

seed:
	cd ./docker/ && docker compose exec $(container) php artisan db:seed --class=$(seeder);

test:
	cd ./docker/;\
	docker compose exec $(db_container) mysql -u root --password=root -e "CREATE DATABASE ingenious_testing";\
	docker compose exec $(container) php artisan --env=testing migrate;\
	docker compose exec $(container) php artisan test;\
	docker compose exec $(db_container) mysql -u root --password=root -e "DROP DATABASE ingenious_testing";

